package org.nuclos.client.rest.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.PathParam;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.message.Message;
import org.nuclos.server.rest.api.databind.BoAttributeDeserializer;
import org.nuclos.server.rest.api.databind.BoAttributeSerializer;
import org.nuclos.server.rest.api.services.BoMetaService;
import org.nuclos.server.rest.api.services.BoService;
import org.nuclos.server.rest.api.services.LoginService;
import org.nuclos.server.rest.api.services.MetaDataService;
import org.nuclos.server.rest.api.services.rvo.AttributeMeta;
import org.nuclos.server.rest.api.services.rvo.Bo;
import org.nuclos.server.rest.api.services.rvo.BoAttribute;
import org.nuclos.server.rest.api.services.rvo.BoList;
import org.nuclos.server.rest.api.services.rvo.BoMeta;
import org.nuclos.server.rest.api.services.rvo.BoMetaSelf;
import org.nuclos.server.rest.api.services.rvo.BoOverview;
import org.nuclos.server.rest.api.services.rvo.BoOverviewList;
import org.nuclos.server.rest.api.services.rvo.Login;
import org.nuclos.server.rest.api.services.rvo.LoginResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

public class Program {
	
	public static void main(String[] args) throws Exception {
		new Program();
	}
	
	public Program() throws Exception {
		
		
        JAXRSClientFactoryBean clientFactory = new JAXRSClientFactoryBean();
        clientFactory.setResourceClass(LoginService.class);
        clientFactory.setAddress("http://localhost:8080/nuclos-war/rest");
        
        // Payload Console Output
        clientFactory.getInInterceptors().add(new LoggingInInterceptor());
        clientFactory.getInFaultInterceptors().add(new LoggingInInterceptor());
        clientFactory.getOutInterceptors().add(new LoggingOutInterceptor());
        clientFactory.getOutFaultInterceptors().add(new LoggingOutInterceptor());
        
        ObjectMapper o = new ObjectMapper();
		SimpleModule module = new SimpleModule("MyModul");
		module.addSerializer(BoAttribute.class, new BoAttributeSerializer());
		module.addDeserializer(BoAttribute.class, new BoAttributeDeserializer());
		o.registerModule(module);
        
        JacksonJsonProvider p = new JacksonJsonProvider(o);
        
        List<Object> providers = new ArrayList<Object>();
        providers.add(p);
        
        
        clientFactory.setProviders(providers );
        
        // Webclient Session Required and Login via Rest-Call Required
        WebClient client = clientFactory.createWebClient();
        WebClient.getConfig(client).getRequestContext().put(Message.MAINTAIN_SESSION, true); 

        LoginService loginService = JAXRSClientFactory.fromClient(client, LoginService.class);
        BoService boService = JAXRSClientFactory.fromClient(client, BoService.class);
        BoMetaService boMetaService = JAXRSClientFactory.fromClient(client, BoMetaService.class);
        MetaDataService metaDataService = JAXRSClientFactory.fromClient(client, MetaDataService.class);
        
        Login login = new Login();
        login.setUsername("nuclos");
        login.setPassword("");
        //login.setLocale("de_DE");
        login.setLocale("en_EN");
        
        LoginResponse response = loginService.login(login);
        
        System.out.println(response.getSessionId());
        
        System.out.println(loginService.dbversion());
        
        System.out.println(loginService.version());
        
        List<BoMeta> bometaList = boService.bometaList();
        System.out.println(bometaList.size());
        
        for (BoMeta boMeta : bometaList) {
        	System.out.println("   "+boMeta.getName()+" "+boMeta.getBoMetaId());
		}
        
        BoMetaSelf metaTest = boMetaService.bometaSelf("org_nuclet_businessentity_TestA");
        Collection<AttributeMeta> attributes = metaTest.getAttributes().values();
        for (AttributeMeta attributeMeta : attributes) {
        	System.out.println(attributeMeta.getName() +" "+ attributeMeta.getScale() +" "+ attributeMeta.getPrecision());
 		}
        
        //TODO IMPL
        //metaDataService.restservices();
        String id = null;
        
        BoOverviewList list = boService.bolist("org_nuclet_businessentity_TestA");
        List<BoOverview> boList = list.getBos();
        for (BoOverview boOverview : boList) {
        	System.out.println("--------------------------------------------------------------");
        	System.out.println(boOverview.getBoId());
        	System.out.println(boOverview.getTitle()+" ::: "+boOverview.getInfo());
        	Bo bo = boService.bodetails("org_nuclet_businessentity_TestA", boOverview.getBoId());
        	System.out.println(bo.getTitle()+" ::: "+bo.getInfo());
        	System.out.println("--------------------------------------------------------------");
        	id = boOverview.getBoId();
		}
        
        System.out.println(list.getBos().size());
        
        
//        Bo bo = boService.bodetails("org_nuclet_businessentity_TestA", id);
//        System.out.println("--------------------------------------------------------------");
//        System.out.println(bo.getBoId());
//    	System.out.println(bo.getTitle()+" ::: "+bo.getInfo());
//    	System.out.println("--------------------------------------------------------------");
    	
    	
        //http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA?chunksize=25&fields=all&gettotal=true&offset=0&search=33&sort=
        BoList list33 = boService.bolist("org_nuclet_businessentity_TestA", 25, "all", true, 0, "33", null);
    	System.out.println(list33.getBos().size());
        
        
        System.out.println(loginService.logout().getStatus());
        
    	
	}
	
}
