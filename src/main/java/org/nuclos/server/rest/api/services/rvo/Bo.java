package org.nuclos.server.rest.api.services.rvo;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


//@JsonIgnoreProperties(ignoreUnknown = true)
public class Bo {
	
	private String boId;
	private String boMetaId;
	private String title;
	private String info;
	
	private  Map<String, BoAttribute> attributes = new HashMap<String, BoAttribute>();
	
	private  Map<String, Map<String,Map<ELinkType, Link>>> subBos = new HashMap<String, Map<String,Map<ELinkType,Link>>>(); 
	
	
	private Map<ELinkType, Link> links = new HashMap<ELinkType, Link>();
	
	public String getBoId() {
		return boId;
	}
	
	public void setBoId(String boId) {
		this.boId = boId;
	}
	
	public String getBoMetaId() {
		return boMetaId;
	}
	
	public void setBoMetaId(String boMetaId) {
		this.boMetaId = boMetaId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public Map<ELinkType, Link> getLinks() {
		return links;
	}
	
	public void setLinks(Map<ELinkType, Link> links) {
		this.links = links;
	}

	public Map<String, Map<String,Map<ELinkType, Link>>> getSubBos() {
		return subBos;
	}

	public void setSubBos(Map<String, Map<String,Map<ELinkType, Link>>> subBos) {
		this.subBos = subBos;
	}

	public Map<String, BoAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, BoAttribute> attributes) {
		this.attributes = attributes;
	}

	
	
}
