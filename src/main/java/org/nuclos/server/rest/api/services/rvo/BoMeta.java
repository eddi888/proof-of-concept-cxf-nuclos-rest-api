package org.nuclos.server.rest.api.services.rvo;

import java.util.HashMap;
import java.util.Map;

public class BoMeta {
	
	private String boMetaId;
	
	private String name;
	
	private Map<ELinkType, Link> links = new HashMap<ELinkType, Link>();

	public String getBoMetaId() {
		return boMetaId;
	}

	public void setBoMetaId(String boMetaId) {
		this.boMetaId = boMetaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<ELinkType, Link> getLinks() {
		return links;
	}

	public void setLinks(Map<ELinkType, Link> links) {
		this.links = links;
	}
	
}
