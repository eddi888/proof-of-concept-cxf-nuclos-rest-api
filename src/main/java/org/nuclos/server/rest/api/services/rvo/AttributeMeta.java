package org.nuclos.server.rest.api.services.rvo;

public class AttributeMeta {
	
	/*
	"boAttrId" : "org_nuclet_businessentity_TestBO001_double92",
	"name" : "double92",
	"type" : "Decimal",
	"readonly" : false,
	"unique" : true,
	"nullable" : false,
	"reference" : false,
	"order" : "1",
	"scale" : 9,
	"precision" : 2
	*/
	
	private String boAttrId;
	private String name;
	private String type;
	private Boolean readonly;
	private Boolean unique;
	private Boolean nullable;
	private Boolean reference;
	private String order;
	private Integer scale;
	private Integer precision;
	private EDefcompType defcomptype; 
	private String referencingBoMetaId;
	
	public String getBoAttrId() {
		return boAttrId;
	}
	
	public void setBoAttrId(String boAttrId) {
		this.boAttrId = boAttrId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Boolean getReadonly() {
		return readonly;
	}
	
	public void setReadonly(Boolean readonly) {
		this.readonly = readonly;
	}
	
	public Boolean getUnique() {
		return unique;
	}
	
	public void setUnique(Boolean unique) {
		this.unique = unique;
	}
	
	public Boolean getNullable() {
		return nullable;
	}
	
	public void setNullable(Boolean nullable) {
		this.nullable = nullable;
	}
	
	public Boolean getReference() {
		return reference;
	}
	
	public void setReference(Boolean reference) {
		this.reference = reference;
	}
	
	public String getOrder() {
		return order;
	}
	
	public void setOrder(String order) {
		this.order = order;
	}
	
	public Integer getScale() {
		return scale;
	}
	
	public void setScale(Integer scale) {
		this.scale = scale;
	}
	
	public Integer getPrecision() {
		return precision;
	}
	
	public void setPrecision(Integer precision) {
		this.precision = precision;
	}

	public EDefcompType getDefcomptype() {
		return defcomptype;
	}

	public void setDefcomptype(EDefcompType defcomptype) {
		this.defcomptype = defcomptype;
	}

	public String getReferencingBoMetaId() {
		return referencingBoMetaId;
	}

	public void setReferencingBoMetaId(String referencingBoMetaId) {
		this.referencingBoMetaId = referencingBoMetaId;
	}
	
	
	
}
