package org.nuclos.server.rest.api.services.rvo;

import java.util.ArrayList;
import java.util.List;

public class BoList {
	
	private Boolean all;
	
	private List<Bo> bos = new ArrayList<Bo>();

	public Boolean getAll() {
		return all;
	}

	public void setAll(Boolean all) {
		this.all = all;
	}

	public List<Bo> getBos() {
		return bos;
	}

	public void setBos(List<Bo> bos) {
		this.bos = bos;
	}

	
	
}
