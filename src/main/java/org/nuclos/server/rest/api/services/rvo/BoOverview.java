package org.nuclos.server.rest.api.services.rvo;

import java.util.HashMap;
import java.util.Map;

public class BoOverview {
	
	private String boId;
	private String boMetaId;
	private String title;
	private String info;
	private Map<String, Link> links = new HashMap<String, Link>();
	
	public String getBoId() {
		return boId;
	}
	
	public void setBoId(String boId) {
		this.boId = boId;
	}
	
	public String getBoMetaId() {
		return boMetaId;
	}
	
	public void setBoMetaId(String boMetaId) {
		this.boMetaId = boMetaId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public Map<String, Link> getLinks() {
		return links;
	}
	
	public void setLinks(Map<String, Link> links) {
		this.links = links;
	}
	
}
