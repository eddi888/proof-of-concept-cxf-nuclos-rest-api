package org.nuclos.server.rest.api.services;

import java.util.List;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.api.services.rvo.Bo;
import org.nuclos.server.rest.api.services.rvo.BoList;
import org.nuclos.server.rest.api.services.rvo.BoMeta;
import org.nuclos.server.rest.api.services.rvo.BoOverviewList;

//import org.nuclos.server.rest.services.helper.DataServiceHelper;
//import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/bos")
@Produces(MediaType.APPLICATION_JSON)
public interface BoService {
	
	@GET
	//@RestServiceInfo(identifier="boMetas", isFinalized=true, description="List of all readable Businessobject-metas")
 	public List<BoMeta> bometaList() ;

	@GET
	@Path("/{boMetaId}")
	//@RestServiceInfo(identifier="bos", isFinalized=true, description="List of Data (Rows)")
	public BoOverviewList bolist(@PathParam("boMetaId") String boMetaId);
	
	//http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA?chunksize=25&fields=all&gettotal=true&offset=0&search=33&sort=
	@GET
	@Path("/{boMetaId}")
	//@RestServiceInfo(identifier="bos", isFinalized=true, description="List of Data (Rows)")
	public BoList bolist(@PathParam("boMetaId") String boMetaId, @QueryParam("chunksize") int chunksize, @QueryParam("fields") String fields, @QueryParam("gettoal") Boolean gettoal, @QueryParam("offset") int offset, @QueryParam("search") String search, @QueryParam("sort") String sort);
	
	@POST
	@Path("/{boMetaId}")
	//@RestServiceInfo(identifier="boinsert", isFinalized=true, description="Data Row Insert")
    @Consumes({MediaType.APPLICATION_JSON})
	public JsonObject boinsert(@PathParam("boMetaId") String boMetaId, JsonObject data);
	
	@GET
	@Path("/{boMetaId}/{boId}")
	//@RestServiceInfo(identifier="bo", isFinalized=true, description="Full Data Row Details")
	public Bo bodetails(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId);
	
	@PUT
	@Path("/{boMetaId}/{boId}")
	//@RestServiceInfo(identifier="boupdate", isFinalized=true, description="Full Data Row Update")
    @Consumes({MediaType.APPLICATION_JSON})
	public JsonObject boupdate(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, JsonObject data);
	
	@DELETE
    @Path("/{boMetaId}/{boId}")
	//@RestServiceInfo(identifier="bodelete", isFinalized=true, description="Full Data Row Delete")
	public Response bodelete(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId);
	
}