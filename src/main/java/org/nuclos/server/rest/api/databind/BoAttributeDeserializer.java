package org.nuclos.server.rest.api.databind;

import java.io.IOException;

import org.nuclos.server.rest.api.services.rvo.BoAttribute;
import org.nuclos.server.rest.api.services.rvo.BoReference;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

public class BoAttributeDeserializer extends JsonDeserializer<BoAttribute> {

	@Override
	public BoAttribute deserialize(JsonParser jp, DeserializationContext cxt) throws IOException, JsonProcessingException {
		BoAttribute boAttribute = new BoAttribute();
		boAttribute.setName(jp.getCurrentName());
		
		//System.err.println();
		JsonNode node = jp.getCodec().readTree(jp);

		JsonNodeType type = node.getNodeType();
		//System.out.println("type:::"+type);
		switch (type) {
		case OBJECT:
			// can be only a BoReference
			boAttribute.setValue(new BoReference(node.get("id").asLong(), node.get("name").asText()));
			break;
		case BOOLEAN:
			boAttribute.setValue(node.asBoolean());
			break;
		case STRING:
			boAttribute.setValue(node.asText());
			break;
		case NUMBER:
			boAttribute.setValue(node.asDouble());
			break;
		default:
			throw new RuntimeException("Unknows Value Type: '"+type+"'");
			
		}
		return boAttribute; 
	}

}
