package org.nuclos.server.rest.api.databind;

import java.io.IOException;

import org.nuclos.server.rest.api.services.rvo.BoAttribute;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class BoAttributeSerializer extends JsonSerializer<BoAttribute> {

	@Override
	public void serialize(BoAttribute value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
		
	}

}
