package org.nuclos.server.rest.api.services.rvo;

public class BoAttribute {
	
	/*
	 			"nummer" : "10001",
				"name" : "Panasonic KX-T7668",
				"artikelgruppe" : {
					"id" : 40004035,
					"name" : "TK"
				},
				"lagerfuehrung" : false,
				"bestellt" : 0.00,
				"reserviert" : 0.00,
				"bestand" : 0.00,
				"einheit" : {
					"id" : 40004022,
					"name" : "Stück"
				},
				"createdAt" : "2015-05-11 12:29:11.647",
				"createdBy" : "nuclos",
				"changedAt" : "2015-05-12 13:17:10.008",
				"changedBy" : "nuclos"
	 */
	enum BoAttributeType{ DOUBLE, INTEGER, REF, BOOLEAN, STRING, DATE }
	
	private String name;
	private Object value;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
}
