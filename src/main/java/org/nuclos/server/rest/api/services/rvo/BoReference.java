package org.nuclos.server.rest.api.services.rvo;

public class BoReference {
	
	private Long id;
	private String name;
	
	public BoReference() {
	}

	public BoReference(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}
