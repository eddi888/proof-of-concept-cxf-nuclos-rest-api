package org.nuclos.server.rest.api.services.rvo;

import java.util.HashMap;
import java.util.Map;

public class LoginResponse {
	
	private String sessionId;
	private String username;
	private String locale;

	private Map<String, Link> links = new HashMap<String, Link>();

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Map<String, Link> getLinks() {
		return links;
	}

	public void setLinks(Map<String, Link> links) {
		this.links = links;
	}

	/*
	links":{ 
	    "boMetas":  {"href":"http://localhost:8080/nuclos-war/rest/bos","methods":["GET"]},
		"menu":{"href":"http://localhost:8080/nuclos-war/rest/meta/menu","methods":["GET"]},
		"tasks":{"href":"http://localhost:8080/nuclos-war/rest/meta/tasks","methods":["GET"]},
		"search":{"href":"http://localhost:8080/nuclos-war/rest/data/search/{text}","methods":["GET"]}
	}
    */
	
}
