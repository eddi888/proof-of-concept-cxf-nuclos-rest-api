package org.nuclos.server.rest.api.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.api.services.rvo.Login;
import org.nuclos.server.rest.api.services.rvo.LoginResponse;

@Path("/")
public interface LoginService {
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public LoginResponse login(Login data);
	
	@DELETE
	public Response logout();
	
	@GET
	@Path("version")
	public String version() ;
	
	@GET
	@Path("version/db")
	public String dbversion();
	
}
