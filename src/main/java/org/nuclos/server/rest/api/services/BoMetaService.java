package org.nuclos.server.rest.api.services;

import javax.json.JsonValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.nuclos.server.rest.api.services.rvo.BoMetaSelf;

//import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/boMetas")
@Produces(MediaType.APPLICATION_JSON)
public interface BoMetaService { //extends MetaDataServiceHelper {
	
	@GET
	@Path("/{boMetaId}")
	//@RestServiceInfo(identifier="boMeta", isFinalized=true, description="Get the meta information about one Businessobject-meta")
	public BoMetaSelf bometaSelf(@PathParam("boMetaId") String boMetaId) ;
	
	@GET
	@Path("/{boMetaId}/subBos/{refAttrId}")
	//@RestServiceInfo(identifier="referencemeta_self", isFinalized=true, description="Get the meta information about one Reference")
	public JsonValue dependencemetaSelf(@PathParam("boMetaId") String boMetaId, @PathParam("refAttrId") String refAttrId) ;
	
}