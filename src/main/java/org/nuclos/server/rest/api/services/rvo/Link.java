package org.nuclos.server.rest.api.services.rvo;

public class Link {
	
	private String href;
	private String[] methods;
	
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String[] getMethods() {
		return methods;
	}
	public void setMethods(String[] methods) {
		this.methods = methods;
	}
	
}
