package org.nuclos.server.rest.api.services.rvo;

public enum ELinkType {
	boMeta, bos, self, defaultGeneration, defaultLayout, layout
}
