package org.nuclos.server.rest.api.services.rvo;

import java.util.ArrayList;
import java.util.List;

public class BoOverviewList {
	
	private Boolean all;
	
	private List<BoOverview> bos = new ArrayList<BoOverview>();

	public Boolean getAll() {
		return all;
	}

	public void setAll(Boolean all) {
		this.all = all;
	}

	public List<BoOverview> getBos() {
		return bos;
	}

	public void setBos(List<BoOverview> bos) {
		this.bos = bos;
	}

	
	
}
