package org.nuclos.server.rest.api.services;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/*import org.apache.commons.io.IOUtils;
import org.jfree.util.Log;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.resource.ResourceResolver;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.rest.NuclosRestApplication;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.TableViewLayout;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.springframework.core.io.Resource;*/

@Path("/meta")
@Produces(MediaType.APPLICATION_JSON)
public interface MetaDataService<T> {
		
	@GET
	@Path("/menu")
	//@RestServiceInfo(identifier="menu", description="List of the menu-items. Includes all Businessobject-metas")
 	public JsonArray menu() ;
	
	@GET
	@Path("/layout/{layoutId}")
	//@RestServiceInfo(identifier="layout", description="Parsed layout for the corresponding layout-id")
 	public JsonObject layout(@PathParam("layoutId") String layoutId) ;
	
	//TODO: TO BE REFACTURED

	@GET
	@Path("/tasks")
	//@RestServiceInfo(identifier="tasks", description="Task-List from searchfilters.")
 	public JsonArray tasks() ;

	@GET
	@Path("/tableviewlayout/{parentUid}/{dependenceUid}")
	//@RestServiceInfo(description="Layout information for the webclient table view.")
	public JsonValue tableviewlayout(@PathParam("parentUid") String parentUid, @PathParam("dependenceUid") String dependenceUid) ;

	@GET
	@Path("/tableviewlayout/{uid}")
	//@RestServiceInfo(description="Layout information for the webclient table view.")
	public JsonValue tableviewlayout(@PathParam("uid") String uid) ;
	
	@GET
	@Path("/searchfilter/{uid}")
	//@RestServiceInfo(description="Get searchfilter.", identifier="searchfilter")
	public JsonObject searchfilter(@PathParam("uid") String uid) ;

	@GET
	@Path("/searchfilters/{uid}")
	//@RestServiceInfo(description="Get all searchfilters for a business object for the logged in user.")
	public JsonArray searchfilters(@PathParam("uid") String uid) ;
	
	@GET
	@Path("/systemparameters")
	public JsonObject systemparameters() ;
	
	@GET
	@Path("/restservices")
	//@RestServiceInfo(isFinalized=true, description="List of restservices.")
	public JsonObject restservices() ;	
	
	@GET
	@Path("/statusicon/{uid}")
	public <PK> Response statusicon(@PathParam("uid") String uid) ;
	
	@GET
	@Path("/icon/{resourcename}")
	//@RestServiceInfo(identifier="resourceicon", description="Resource-Icon")
	public <PK> Response icon(@PathParam("resourcename") String resourcename) ;
	
	@GET
	@Path("/sideviewmenuselector")
	//@RestServiceInfo(identifier="sideviewmenu", description="Sideview Menu Buttons. (List of 'neverClose' tabs.)")
	public JsonArray sideviewMenuSelector() ;

}