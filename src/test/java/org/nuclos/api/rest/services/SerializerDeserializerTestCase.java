/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package org.nuclos.api.rest.services;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.nuclos.server.rest.api.databind.BoAttributeDeserializer;
import org.nuclos.server.rest.api.databind.BoAttributeSerializer;
import org.nuclos.server.rest.api.services.rvo.AttributeMeta;
import org.nuclos.server.rest.api.services.rvo.Bo;
import org.nuclos.server.rest.api.services.rvo.BoAttribute;
import org.nuclos.server.rest.api.services.rvo.BoMeta;
import org.nuclos.server.rest.api.services.rvo.BoMetaSelf;
import org.nuclos.server.rest.api.services.rvo.BoOverview;
import org.nuclos.server.rest.api.services.rvo.BoOverviewList;
import org.nuclos.server.rest.api.services.rvo.ELinkType;
import org.nuclos.server.rest.api.services.rvo.Link;
import org.nuclos.server.rest.api.services.rvo.Login;
import org.nuclos.server.rest.api.services.rvo.LoginResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class SerializerDeserializerTestCase {
	ObjectMapper o ;
	
	@Before
	public void setUp(){
		
		o = new ObjectMapper();
		SimpleModule module = new SimpleModule("MyModul");
		module.addSerializer(BoAttribute.class, new BoAttributeSerializer());
		module.addDeserializer(BoAttribute.class, new BoAttributeDeserializer());
		o.registerModule(module);
		
		
	}
    @Test
    public void testLogin() throws Exception {
        File in = new File("src/test/resources/example-nuclos/login-request.json");
        File out = new File("src/test/resources/example-nuclos/login-response.json"); 
        //Object login = (PrestashopAddress) JAXBContext.newInstance(JsonObject.class).createUnmarshaller().unmarshal(in);

        Login inObj = new ObjectMapper().readValue(in, Login.class);

        LoginResponse outObj = new ObjectMapper().readValue(out, LoginResponse.class);

        
        /*        Assert.assertNotNull(prestashop);
        Assert.assertNotNull(prestashop.getAddress());
        Assert.assertNotNull(prestashop.getAddress());
        Assert.assertNotNull(prestashop.getAddress().getId());
        Assert.assertNotNull(prestashop.getAddress().getIdCustomer());
  */      
/*        Marshaller m = JAXBContext.newInstance(PrestashopAddress.class).createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(prestashop, out); 
  */      
        
    }
    

    @Test
    public void testBoMetas() throws Exception {
        //File in = new File("src/test/resources/example-nuclos/login-request.json");
        File out = new File("src/test/resources/example-nuclos/bos-response.json"); 
        //Object login = (PrestashopAddress) JAXBContext.newInstance(JsonObject.class).createUnmarshaller().unmarshal(in);

        //Login inObj = new ObjectMapper().readValue(in, Login.class);

        BoMeta[] outObj = new ObjectMapper().readValue(out, BoMeta[].class);
        for (BoMeta boMeta : outObj) {
			System.out.println(boMeta.getName());
		}
        
        /*        Assert.assertNotNull(prestashop);
        Assert.assertNotNull(prestashop.getAddress());
        Assert.assertNotNull(prestashop.getAddress());
        Assert.assertNotNull(prestashop.getAddress().getId());
        Assert.assertNotNull(prestashop.getAddress().getIdCustomer());
  */      
/*        Marshaller m = JAXBContext.newInstance(PrestashopAddress.class).createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(prestashop, out); 
  */      
        
    }
    
    @Test
    public void testBosArtikel() throws Exception {
        File out = new File("src/test/resources/example-nuclos/bos-org_nuclet_basistemplate_Artikel-response.json");
        
        BoOverviewList outObj = new ObjectMapper().readValue(out, BoOverviewList.class);
	    List<BoOverview> bos = outObj.getBos();
	    for (BoOverview bo : bos) {
			System.out.println(bo.getTitle()+" "+bo.getInfo());
		}
    
	    
    }
    
    
    @Test
    public void testBoMetasTestBO001() throws Exception {
    	File out = new File("src/test/resources/example-nuclos/boMetas-org_nuclet_businessentity_TestBO001.js");
        
    	BoMetaSelf outObj = new ObjectMapper().readValue(out, BoMetaSelf.class);
        System.out.println(outObj.getAttributes());
        Set<String> boMetaKeys = outObj.getAttributes().keySet();
        for (String boMetaKey : boMetaKeys) {
        	AttributeMeta meta = outObj.getAttributes().get(boMetaKey);
        	System.out.println("  "+meta.getBoAttrId());
        	System.out.println("  "+meta.getName());
        	System.out.println("  "+meta.getNullable());
        	
		}
		
		//System.out.println( outObj.getSubBos().get("org_nuclet_basistemplate_ArtikelEKPreis_artikel"));
		//System.out.println(outObj.getSubBos().get("org_nuclet_basistemplate_ArtikelEKPreis_artikel").get("links"));
        
    }
    
    
    
    
    
    @Test
    public void testBosArtikel40004036() throws Exception {
    	File out = new File("src/test/resources/example-nuclos/bos-org_nuclet_basistemplate_Artikel-40004036-GET-RESPONSE.js");
        
        Bo outObj = o.readValue(out, Bo.class);
        
		System.out.println( outObj.getSubBos().get("org_nuclet_basistemplate_ArtikelEKPreis_artikel"));
		
		System.out.println(outObj.getSubBos().get("org_nuclet_basistemplate_ArtikelEKPreis_artikel").get("links").get(ELinkType.self));
		
		Map<String, Map<String, Map<ELinkType, Link>>> subBos = outObj.getSubBos();
		
		for (String key : subBos.keySet()) {
			System.out.println(" "+key);
			Map<String, Map<ELinkType, Link>> subBo = subBos.get(key);
			
			for (String key2 : subBo.keySet()) {
				System.out.println("   "+key2);
					Map<ELinkType, Link> links = subBo.get(key2);
					Set<ELinkType> linkKey = links.keySet();
					System.out.println("     "+linkKey);
			}
		}
		
		Set<ELinkType> links = outObj.getLinks().keySet();
		for (ELinkType linkKey : links) {
			outObj.getLinks().get(linkKey);
			System.out.println(linkKey);
		}
		
		Collection<BoAttribute> attributes = outObj.getAttributes().values();
		for (BoAttribute boAttribute : attributes) {
			System.out.println(boAttribute.getName()+": "+boAttribute.getValue());
		}
		
		
    }
    
    @Test
    public void testBosTestA40004185() throws Exception {
    	File out = new File("src/test/resources/example-nuclos/bos-org_nuclet_businessentity_TestA-40004185_GET-RESPONSE.js");
    	Bo outObj = o.readValue(out, Bo.class);
        
    	
    }
    
    @Test
    public void testBosTestA40000061() throws Exception {
    	File out = new File("src/test/resources/example-nuclos/bos-org_nuclet_businessentity_TestA-40000061_GET-RESPONSE.js");
    	Bo outObj = o.readValue(out, Bo.class);
        
    	
    }
    
    
}
