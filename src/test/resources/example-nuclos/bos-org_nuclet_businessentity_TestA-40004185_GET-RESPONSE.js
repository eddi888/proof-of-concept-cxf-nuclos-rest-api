{
	"boId" : 40004185,
	"boMetaId" : "org_nuclet_businessentity_TestA",
	"title" : "10 - 8,00",
	"info" : "7.0000, NuclosPassword [null], 1",
	"attributes" : {
		"integer9" : 10,
		"double92" : 8.00,
		"double94" : 7.0000,
		"boolean1" : false,
		"password255" : "NuclosPassword [null]",
		"updateruletest" : "1",
		"updatefinalruletest" : "23",
		"insertruletest" : "456",
		"insertfinalruletest" : "7890"
	},
	"subBos" : {
		"org_nuclet_businessentity_TestB_ref255" : {
			"links" : {
				"boMeta" : {
					"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestA/subBos/org_nuclet_businessentity_TestB_ref255",
					"methods" : [ "GET" ]
				},
				"bos" : {
					"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004185/subBos/org_nuclet_businessentity_TestB_ref255",
					"methods" : [ "GET" ]
				}
			}
		}
	},
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004185",
			"methods" : [ "GET", "PUT", "DELETE" ]
		},
		"boMeta" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestA",
			"methods" : [ "GET" ]
		},
		"layout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_layout_TestALO",
			"methods" : [ "GET" ]
		}
	}
}
