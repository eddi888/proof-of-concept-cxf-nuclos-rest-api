{
	"boId" : 40004036,
	"boMetaId" : "org_nuclet_basistemplate_Artikel",
	"title" : "10001",
	"info" : "Panasonic KX-T7668, TK, 0.00",
	"attributes" : {
		"nummer" : "10001",
		"name" : "Panasonic KX-T7668",
		"artikelgruppe" : {
			"id" : 40004035,
			"name" : "TK"
		},
		"lagerfuehrung" : false,
		"bestellt" : 0.00,
		"reserviert" : 0.00,
		"bestand" : 0.00,
		"einheit" : {
			"id" : 40004022,
			"name" : "Stück"
		},
		"createdAt" : "2015-05-11 12:29:11.647",
		"createdBy" : "nuclos",
		"changedAt" : "2015-05-12 13:17:10.008",
		"changedBy" : "nuclos"
	},
	"subBos" : {
		"org_nuclet_lager_Lagerbestand_artikel" : {
			"links" : {
				"boMeta" : {
					"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_basistemplate_Artikel/subBos/org_nuclet_lager_Lagerbestand_artikel",
					"methods" : [ "GET" ]
				},
				"bos" : {
					"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_Artikel/40004036/subBos/org_nuclet_lager_Lagerbestand_artikel",
					"methods" : [ "GET" ]
				}
			}
		},
		"org_nuclet_basistemplate_ArtikelVKPreis_artikel" : {
			"links" : {
				"boMeta" : {
					"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_basistemplate_Artikel/subBos/org_nuclet_basistemplate_ArtikelVKPreis_artikel",
					"methods" : [ "GET" ]
				},
				"bos" : {
					"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_Artikel/40004036/subBos/org_nuclet_basistemplate_ArtikelVKPreis_artikel",
					"methods" : [ "GET" ]
				}
			}
		},
		"org_nuclet_basistemplate_ArtikelEKPreis_artikel" : {
			"links" : {
				"boMeta" : {
					"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_basistemplate_Artikel/subBos/org_nuclet_basistemplate_ArtikelEKPreis_artikel",
					"methods" : [ "GET" ]
				},
				"bos" : {
					"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_Artikel/40004036/subBos/org_nuclet_basistemplate_ArtikelEKPreis_artikel",
					"methods" : [ "GET" ]
				}
			}
		}
	},
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_Artikel/40004036",
			"methods" : [ "GET", "PUT", "DELETE" ]
		},
		"boMeta" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_basistemplate_Artikel",
			"methods" : [ "GET" ]
		},
		"layout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_basistemplate_ArtikelLO",
			"methods" : [ "GET" ]
		}
	}
}