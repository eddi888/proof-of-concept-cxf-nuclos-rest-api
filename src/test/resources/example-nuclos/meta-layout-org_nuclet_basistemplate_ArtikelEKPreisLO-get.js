{
	"layout_id" : "org_nuclet_basistemplate_ArtikelEKPreisLO",
	"type" : "ROOT",
	"cstr" : null,
	"layout" : {
		"columns" : [ 12.0, 84.0, 168.0, 168.0, -1.0, 5.0 ],
		"rows" : [ 12.0, 22.0, 22.0, 22.0, 22.0, 22.0, 5.0 ]
	},
	"components" : [ {
		"type" : "LABEL",
		"text" : "Artikel",
		"cstr" : {
			"column" : 1,
			"row" : 1,
			"size" : {
				"minimun" : {
					"width" : 15,
					"height" : 22
				},
				"preferred" : {
					"width" : 50,
					"height" : 22
				}
			}
		}
	}, {
		"uid" : "org_nuclet_basistemplate_ArtikelEKPreis_artikel",
		"fieldname" : "artikel",
		"name" : "Artikel",
		"readonly" : false,
		"cstr" : {
			"column" : 2,
			"row" : 1,
			"size" : {
				"minimun" : {
					"width" : 35,
					"height" : 22
				},
				"preferred" : {
					"width" : 70,
					"height" : 22
				}
			}
		}
	}, {
		"type" : "LABEL",
		"text" : "Lieferant",
		"cstr" : {
			"column" : 1,
			"row" : 2,
			"size" : {
				"minimun" : {
					"width" : 15,
					"height" : 22
				},
				"preferred" : {
					"width" : 50,
					"height" : 22
				}
			}
		}
	}, {
		"uid" : "org_nuclet_basistemplate_ArtikelEKPreis_lieferant",
		"fieldname" : "lieferant",
		"name" : "Lieferant",
		"readonly" : false,
		"cstr" : {
			"column" : 2,
			"row" : 2,
			"size" : {
				"minimun" : {
					"width" : 35,
					"height" : 22
				},
				"preferred" : {
					"width" : 70,
					"height" : 22
				}
			}
		}
	}, {
		"type" : "LABEL",
		"text" : "Preis",
		"cstr" : {
			"column" : 1,
			"row" : 3,
			"size" : {
				"minimun" : {
					"width" : 15,
					"height" : 22
				},
				"preferred" : {
					"width" : 50,
					"height" : 22
				}
			}
		}
	}, {
		"uid" : "org_nuclet_basistemplate_ArtikelEKPreis_preis",
		"fieldname" : "preis",
		"name" : "Preis",
		"readonly" : false,
		"cstr" : {
			"column" : 2,
			"colspan" : 2,
			"row" : 3,
			"valign" : "top",
			"size" : {
				"minimun" : {
					"width" : 35,
					"height" : 22
				},
				"preferred" : {
					"width" : 70,
					"height" : 22
				}
			}
		}
	}, {
		"type" : "LABEL",
		"text" : "Staffelmenge",
		"cstr" : {
			"column" : 1,
			"row" : 4,
			"size" : {
				"minimun" : {
					"width" : 15,
					"height" : 22
				},
				"preferred" : {
					"width" : 50,
					"height" : 22
				}
			}
		}
	}, {
		"uid" : "org_nuclet_basistemplate_ArtikelEKPreis_staffelmenge",
		"fieldname" : "staffelmenge",
		"name" : "Staffelmenge",
		"readonly" : false,
		"cstr" : {
			"column" : 2,
			"colspan" : 2,
			"row" : 4,
			"valign" : "top",
			"size" : {
				"minimun" : {
					"width" : 35,
					"height" : 22
				},
				"preferred" : {
					"width" : 70,
					"height" : 22
				}
			}
		}
	}, {
		"type" : "LABEL",
		"text" : "WÃ¤hrung",
		"cstr" : {
			"column" : 1,
			"row" : 5,
			"size" : {
				"minimun" : {
					"width" : 15,
					"height" : 22
				},
				"preferred" : {
					"width" : 50,
					"height" : 22
				}
			}
		}
	}, {
		"uid" : "org_nuclet_basistemplate_ArtikelEKPreis_waehrung",
		"fieldname" : "waehrung",
		"name" : "Währung",
		"readonly" : false,
		"cstr" : {
			"column" : 2,
			"row" : 5,
			"size" : {
				"strict" : {
					"width" : 70,
					"height" : 20
				}
			}
		}
	} ]
}
