{
	"boId" : 40004120,
	"boMetaId" : "org_nuclet_basistemplate_Artikel",
	"title" : "1",
	"info" : "TEST2, TK, Stück",
	"attributes" : {
		"nummer" : "1",
		"name" : "TEST2",
		"artikelgruppe" : {
			"id" : 40004035,
			"name" : "TK"
		},
		"lagerfuehrung" : true,
		"einheit" : {
			"id" : 40004022,
			"name" : "Stück"
		},
		"createdAt" : "2015-05-12 16:44:06.642",
		"createdBy" : "nuclos",
		"changedAt" : "2015-05-12 16:54:15.686",
		"changedBy" : "nuclos",
		"undefined" : false
	},
	"subBos" : {
		"insert" : {
			"org_nuclet_basistemplate_ArtikelVKPreis_artikel" : [ {
				"boId" : null,
				"boMetaId" : "org_nuclet_basistemplate_ArtikelVKPreis",
				"attributes" : {
					"preis" : 3,
					"kunde" : {
						"id" : 40004045,
						"name" : "20002 Scala Kino Lichtspiele"
					},
					"artikel" : {
						"id" : null,
						"name" : null
					},
					"waehrung" : {
						"id" : 40004028,
						"name" : "EUR"
					},
					"staffelmenge" : 1
				},
				"links" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_ArtikelVKPreis",
				"$promise" : {},
				"$resolved" : true,
				"dropdownoptions" : {
					"artikel" : [],
					"kunde" : [ {
						"id" : 40004043,
						"name" : "20001 Deutsche Bahn"
					}, {
						"id" : 40004045,
						"name" : "20002 Scala Kino Lichtspiele"
					} ],
					"waehrung" : [ {
						"id" : 40004028,
						"name" : "EUR"
					}, {
						"id" : 40004029,
						"name" : "THB"
					} ]
				},
				"_flag" : "insert",
				"_restrictions" : {},
				"_layout" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_basistemplate_ArtikelVKPreisLO",
				"_flagSave" : "insert",
				"params" : {
					"d" : 1431443556655
				}
			} ],
			"org_nuclet_basistemplate_ArtikelEKPreis_artikel" : [ {
				"boId" : null,
				"boMetaId" : "org_nuclet_basistemplate_ArtikelEKPreis",
				"attributes" : {
					"waehrung" : {
						"id" : 40004028,
						"name" : "EUR"
					},
					"artikel" : {
						"id" : null,
						"name" : null
					},
					"staffelmenge" : 4,
					"lieferant" : {
						"id" : 40004081,
						"name" : "30001 Panasonic Germany"
					},
					"preis" : 2
				},
				"links" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_ArtikelEKPreis",
				"$promise" : {},
				"$resolved" : true,
				"dropdownoptions" : {
					"artikel" : [],
					"lieferant" : [ {
						"id" : 40004081,
						"name" : "30001 Panasonic Germany"
					} ],
					"waehrung" : [ {
						"id" : 40004028,
						"name" : "EUR"
					}, {
						"id" : 40004029,
						"name" : "THB"
					} ]
				},
				"_flag" : "insert",
				"_restrictions" : {},
				"_layout" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_basistemplate_ArtikelEKPreisLO",
				"_flagSave" : "insert",
				"params" : {
					"d" : 1431443541241
				}
			} ]
		},
		"update" : {},
		"delete" : {}
	},
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_basistemplate_Artikel/40004120",
			"methods" : [ "GET", "PUT", "DELETE" ]
		},
		"boMeta" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_basistemplate_Artikel",
			"methods" : [ "GET" ]
		},
		"layout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_basistemplate_ArtikelLO",
			"methods" : [ "GET" ]
		}
	},
	"$promise" : {},
	"$resolved" : true,
	"dropdownoptions" : {},
	"_flag" : "update",
	"params" : {
		"d" : 1431443937366
	}
}