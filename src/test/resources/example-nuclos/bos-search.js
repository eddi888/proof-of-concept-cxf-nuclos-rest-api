{
	"all" : true,
	"bos" : [
			{
				"boId" : 40004176,
				"boMetaId" : "org_nuclet_businessentity_TestA",
				"attributes" : {
					"integer9" : 4,
					"double92" : 33.00,
					"double94" : 3.0000,
					"boolean1" : false,
					"password255" : "NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [null]]]]",
					"updateruletest" : "UpdateRule: Integer9:4 Now:Thu Jun 04 14:17:16 CEST 2015"
				},
				"links" : {
					"self" : {
						"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004176",
						"methods" : [ "GET", "PUT", "DELETE" ]
					}
				}
			},
			{
				"boId" : 40004197,
				"boMetaId" : "org_nuclet_businessentity_TestA",
				"attributes" : {
					"integer9" : 123456789,
					"double92" : 1234567.89,
					"double94" : 12345.6789,
					"date" : "1998-12-31",
					"password255" : "NuclosPassword [NuclosPassword [null]]",
					"updateruletest" : "UpdateRule: Integer9:123456789 Now:Fri Jun 05 14:16:36 CEST 2015",
					"insertruletest" : "InsertRule: Integer9:123456789 Now:Wed Jun 03 17:51:41 CEST 2015"
				},
				"links" : {
					"self" : {
						"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004197",
						"methods" : [ "GET", "PUT", "DELETE" ]
					}
				}
			},
			{
				"boId" : 40004189,
				"boMetaId" : "org_nuclet_businessentity_TestA",
				"attributes" : {
					"integer9" : 15,
					"double92" : 16.70,
					"double94" : 17.8900,
					"password255" : "NuclosPassword [NuclosPassword [NuclosPassword [null]]]",
					"updateruletest" : "UpdateRule: Integer9:15 Now:Fri Jun 05 13:34:25 CEST 2015",
					"insertruletest" : "InsertRule: Integer9:14 Now:Wed Jun 03 11:15:13 CEST 2015"
				},
				"links" : {
					"self" : {
						"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004189",
						"methods" : [ "GET", "PUT", "DELETE" ]
					}
				}
			},
			{
				"boId" : 40004187,
				"boMetaId" : "org_nuclet_businessentity_TestA",
				"attributes" : {
					"integer9" : 13,
					"double92" : 12.30,
					"double94" : 11.2340,
					"boolean1" : true,
					"password255" : "NuclosPassword [NuclosPassword [NuclosPassword [null]]]",
					"updateruletest" : "UpdateRule: Integer9:13 Now:Fri Jun 05 14:17:00 CEST 2015",
					"insertruletest" : "InsertRule: Integer9:12 Now:Wed Jun 03 11:14:23 CEST 2015"
				},
				"links" : {
					"self" : {
						"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004187",
						"methods" : [ "GET", "PUT", "DELETE" ]
					}
				}
			},
			{
				"boId" : 40004185,
				"boMetaId" : "org_nuclet_businessentity_TestA",
				"attributes" : {
					"integer9" : 10,
					"double92" : 8.00,
					"double94" : 7.0000,
					"boolean1" : false,
					"password255" : "NuclosPassword [null]",
					"updateruletest" : "1",
					"updatefinalruletest" : "23",
					"insertruletest" : "456",
					"insertfinalruletest" : "7890"
				},
				"links" : {
					"self" : {
						"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40004185",
						"methods" : [ "GET", "PUT", "DELETE" ]
					}
				}
			} ]
}