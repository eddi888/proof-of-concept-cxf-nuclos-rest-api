 {
	"boMetaId" : "org_nuclet_businessentity_TestA",
	"name" : "TestA",
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestA",
			"methods" : [ "GET" ]
		},
		"defaultGeneration" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boGenerations/org_nuclet_businessentity_TestA",
			"methods" : [ "GET" ]
		},
		"defaultLayout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_layout_TestALO",
			"methods" : [ "GET" ]
		}
	},
	"attributes" : {
		"integer9" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_integer9",
			"name" : "integer9",
			"type" : "Integer",
			"readonly" : false,
			"unique" : true,
			"nullable" : true,
			"reference" : false,
			"order" : "1",
			"scale" : 9
		},
		"double92" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_double92",
			"name" : "double92",
			"type" : "Decimal",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "2",
			"scale" : 9,
			"precision" : 2
		},
		"double94" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_double94",
			"name" : "double94",
			"type" : "Decimal",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "3",
			"scale" : 9,
			"precision" : 4
		},
		"date" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_date",
			"name" : "date",
			"type" : "Date",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "4"
		},
		"hyperlink1000" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_hyperlink1000",
			"name" : "hyperlink1000",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "5",
			"defcomptype" : "Hyperlink",
			"scale" : 1000
		},
		"email255" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_email255",
			"name" : "email255",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "6",
			"defcomptype" : "Email",
			"scale" : 255
		},
		"boolean1" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_boolean1",
			"name" : "boolean1",
			"type" : "Boolean",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "7",
			"scale" : 1
		},
		"password255" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_password255",
			"name" : "password255",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "8",
			"scale" : 255
		},
		"memo4000" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_memo4000",
			"name" : "memo4000",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "9",
			"scale" : 4000
		},
		"updateruletest" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_updateruletest",
			"name" : "TestToken",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "10",
			"scale" : 255
		},
		"updatefinalruletest" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_updatefinalruletest",
			"name" : "UpdateFinalRuleTest",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "11",
			"scale" : 255
		},
		"insertruletest" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_insertruletest",
			"name" : "InsertRuleTest",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "12",
			"scale" : 255
		},
		"insertfinalruletest" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_insertfinalruletest",
			"name" : "InsertFinalRuleTest",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "13",
			"scale" : 255
		},
		"customruletest" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_customruletest",
			"name" : "CustomRuleTest",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "14",
			"scale" : 255
		},
		"treeref" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_treeref",
			"name" : "treeref",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : true,
			"order" : "15",
			"defcomptype" : "Combobox",
			"referencingBoMetaId" : "org_nuclet_businessentity_TestATree",
			"scale" : 255
		},
		"createdAt" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_createdAt",
			"name" : "Created at",
			"type" : "Timestamp",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65510"
		},
		"createdBy" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_createdBy",
			"name" : "Created by",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65511",
			"scale" : 255
		},
		"changedAt" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_changedAt",
			"name" : "Changed at",
			"type" : "Timestamp",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65512"
		},
		"changedBy" : {
			"boAttrId" : "org_nuclet_businessentity_TestA_changedBy",
			"name" : "Changed by",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65513",
			"scale" : 255
		}
	}
}
