{
	"boId" : 40000061,
	"boMetaId" : "org_nuclet_businessentity_TestA",
	"title" : "7 - 8,12",
	"info" : "8.3450, 24.12.2014, http://www.google.de",
	"attributes" : {
		"integer9" : 7,
		"double92" : 8.12,
		"double94" : 8.3450,
		"date" : "2014-12-24",
		"hyperlink1000" : "http://www.google.de",
		"email255" : "eddi888@atomspace.org",
		"boolean1" : false,
		"password255" : "NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword [NuclosPassword []]]]]]]]]]]]",
		"memo4000" : "MEMO",
		"updateruletest" : "UpdateRule: Integer9:7 Now:Thu Jun 04 14:17:02 CEST 2015"
	},
	"subBos" : {
		"org_nuclet_businessentity_TestB_ref255" : {
			"links" : {
				"boMeta" : {
					"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestA/subBos/org_nuclet_businessentity_TestB_ref255",
					"methods" : [ "GET" ]
				},
				"bos" : {
					"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40000061/subBos/org_nuclet_businessentity_TestB_ref255",
					"methods" : [ "GET" ]
				}
			}
		}
	},
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/bos/org_nuclet_businessentity_TestA/40000061",
			"methods" : [ "GET", "PUT", "DELETE" ]
		},
		"boMeta" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestA",
			"methods" : [ "GET" ]
		},
		"layout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_layout_TestALO",
			"methods" : [ "GET" ]
		}
	}
}
