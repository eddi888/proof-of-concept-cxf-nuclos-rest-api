{
	"boMetaId" : "org_nuclet_businessentity_TestBO001",
	"name" : "TestBO001",
	"links" : {
		"self" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boMetas/org_nuclet_businessentity_TestBO001",
			"methods" : [ "GET" ]
		},
		"defaultGeneration" : {
			"href" : "http://localhost:8080/nuclos-war/rest/boGeneration/org_nuclet_businessentity_TestBO001",
			"methods" : [ "GET" ]
		},
		"defaultLayout" : {
			"href" : "http://localhost:8080/nuclos-war/rest/meta/layout/org_nuclet_layout_TestBO001LO",
			"methods" : [ "GET" ]
		}
	},
	"attributes" : {
		"double92" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_double92",
			"name" : "double92",
			"type" : "Decimal",
			"readonly" : false,
			"unique" : true,
			"nullable" : false,
			"reference" : false,
			"order" : "1",
			"scale" : 9,
			"precision" : 2
		},
		"double94" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_double94",
			"name" : "double94",
			"type" : "Decimal",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "2",
			"scale" : 9,
			"precision" : 4
		},
		"integer9" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_integer9",
			"name" : "integer9",
			"type" : "Integer",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "3",
			"scale" : 9
		},
		"text255" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_text255",
			"name" : "text255",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "4",
			"scale" : 255
		},
		"date" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_date",
			"name" : "date",
			"type" : "Date",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "5"
		},
		"password255" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_password255",
			"name" : "password255",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "6",
			"scale" : 255
		},
		"meno4000" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_meno4000",
			"name" : "meno4000",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "7",
			"scale" : 4000
		},
		"createdAt" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_createdAt",
			"name" : "Created at",
			"type" : "Timestamp",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65510"
		},
		"createdBy" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_createdBy",
			"name" : "Created by",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65511",
			"scale" : 255
		},
		"changedAt" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_changedAt",
			"name" : "Changed at",
			"type" : "Timestamp",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65512"
		},
		"changedBy" : {
			"boAttrId" : "org_nuclet_businessentity_TestBO001_changedBy",
			"name" : "Changed by",
			"type" : "String",
			"readonly" : false,
			"unique" : false,
			"nullable" : true,
			"reference" : false,
			"order" : "65513",
			"scale" : 255
		}
	}
}